﻿using System;

namespace Gtk.EventDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Init();
            var win = new Window("Gtk.EventDemo");
            win.SetDefaultSize(250, 100);
            win.SetPosition(WindowPosition.Center);
            win.DeleteEvent += (s, e) =>
            {
                Application.Quit();
            };
            var lbl = new Label("标签内容");
            var box = new EventBox();
            box.Add(lbl);
            box.ButtonPressEvent += (s, e) => { Console.WriteLine("标签被点击：" + e); };
            win.Add(box);
            win.ShowAll();
            Application.Run();
        }
    }
}
