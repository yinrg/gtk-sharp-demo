﻿using System;

namespace Gtk.Relay
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Init();
            var frm = new FrmMain();
            frm.ShowAll();
            Application.Run();
        }
    }
}
